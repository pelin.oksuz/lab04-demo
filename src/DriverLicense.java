public class DriverLicense {

    private String driverName;
    private String driverID;
    private String expirationDate;
    private String bloodType;
    private int penalty;
    private Boolean status;

    final static int limit = 100;

    public DriverLicense(String driverName, String driverID, String expirationDate, String bloodType){

        this.driverName = driverName;
        this.driverID = driverID;
        this.expirationDate = expirationDate;
        this.bloodType = bloodType;

        this.penalty = 0;
        this.status = true;

    }

    public void increasePenalty(int points){
        this.penalty += points;
        setStatus();
    }

    public void decreasePenalty(int points){
        if(this.penalty - points > 0)
            this.penalty -= points;
        else
            this.penalty = 0;
        setStatus();
    }

    public void setStatus(){
        if(penalty >= 100){
            status = false;
        }
        else if(!status && penalty < 20){
            status = true;
        }
    }

    public void setExpirationDate(String expirationDate){
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "DriverLicense{" +
                "driverName='" + driverName + '\'' +
                ", driverID='" + driverID + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                ", bloodType='" + bloodType + '\'' +
                ", penalty=" + penalty +
                ", status=" + status +
                ", limit=" + limit +
                '}';
    }
}
